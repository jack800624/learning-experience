<script> <!-- 编写script标签是为了编辑器识别js代码，可以省略 -->  

<?php 
$this->beginBlock('myjs') ?>
 
$(document).ready(function(){$("#signupform-verifycode-image").click();}); 
 
<?php $this->endBlock(); ?>  
</script>  

<?php $this->registerJs($this->blocks['myjs'],\yii\web\View::POS_LOAD);//将编写的js代码注册到页面底部 ?> 
